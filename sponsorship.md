---
title: Sponsoring KVM Forum
layout: page
permalink: /sponsorship/
---
{% include relative_root.html %}

Sponsoring KVM Forum helps bringing together a diverse community of
developers for Linux-based virtualization and emulation techologies,
including KVM, QEMU, VFIO, virtio and more.

Sponsorships help making the conference successful and affordable,
and demonstrate support for the work of the open source virtualization
community.

Three levels of sponsorships are available:

<div class="sponsorship">
{% markdown sponsorship-levels.md %}
</div>

If you are interested in sponsoring KVM Forum, please check out the
[sponsorship agreement]({{ relative_root }}/assets/2025-sponsorship-agreement-v2.pdf)
(also available as [.odt]({{ relative_root }}/assets/2025-sponsorship-agreement-v2.odt))
and contact the organizers at [kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com).

If you are interested in sponsoring KVM Forum, please contact the organizers
at [kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com).

{% comment %}
{% include sponsors.html %}
{% endcomment %}
