---
name: Joe Brockmeier
role: Supervisor, Community Management - Red Hat
---
Joe is a long-time participant in open source projects and former
technology journalist. He has worked as the openSUSE Community Manager,
is an Apache Software Foundation (ASF) member, and participates heavily
in the Fedora Cloud Working Group. Brockmeier works for Red Hat in the
Open Source and Standards (OSAS) department as the Community Team Manager.
