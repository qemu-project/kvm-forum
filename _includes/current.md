{% assign edition = site.editions[-1] %}

{% if edition.register %}
## Join us for KVM Forum {{ edition.title }}
{% else %}
## Announcing KVM Forum {{ edition.title }}
{% endif %}

KVM Forum is an annual event that presents a rare opportunity for
developers and users to discuss the state of Linux virtualization
technology and plan for the challenges ahead. Sessions include updates
on the state of the KVM virtualization stack, planning for the future,
and many opportunities for attendees to collaborate.  Birds of a feather
(BoF) sessions facilitate discussion of strategic decisions.

{% if edition.schedule %}

## KVM Forum {{ edition.title }} schedule now available!

The next KVM Forum will be held in **[{{ edition.place }}](/location)**
on **{{ edition.dates | replace: "-", "&ndash;" }}**.
**[Consult the schedule here]({{ edition.schedule }})**.

<!--
Speakers should upload their slides on [Pretalx]({{ edition.submit }}).
To do so, edit your submission and add the slides as a "resource".
Thanks!
-->

{% elsif edition.place %}

The next KVM Forum will be held in **[{{ edition.place }}](/location)**
on **{{ edition.dates | replace: "-", "&ndash;" }}**.

{% if edition.submit %}
## Deadlines

Proposals for speaking at KVM Forum should be submitted on [Pretalx]({{edition.submit}}).
The **deadline** for submitting presentations is **... - 11:59 PM PDT**.

Accepted speakers will be notified on **...**.
{% endif %}

{% else %}

Stay tuned for the dates and location of the next KVM Forum!

{% endif %}

## Attending KVM Forum

{% if edition.register %}
The cost of admission to KVM Forum {{ edition.title }} is $75
<!-- until ..., and $100 afterwards -->. You can register
[here]({{edition.register}}).  Admission is free for accepted speakers.

**If you need a visa invitation letter** in order
to attend, please reach out to the organizers at
[kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com).

<!--
Special rates will be available at .... for attendees of KVM Forum.
More information will be available soon
-->

<!--
Special room prices are available at ... for attendees of
KVM Forum. To book a room, please fill out the [reservation form]().
-->

<!--
Special room prices are available at ... for attendees of
KVM Forum. To book a room, please use the coupon code that you
received when registering.
-->

<!--
Booking at the special conference rate at ... is now limited to speakers
and groups.  Please reach out to the organizers at
[kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com) to
ask about availability.
-->

<!--
Booking at the special conference rate at ... is now closed.  If you have
booked a room and wish to cancel, please reach out to the organizers at
[kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com).
-->

{% else %}
Registration to KVM Forum {{ edition.title }} is not open yet.
{% endif %}

We are committed to fostering an open and welcoming environment at our
conference. Participants are expected to abide by our [code
of conduct](coc/) and [media policy](media-policy/).
