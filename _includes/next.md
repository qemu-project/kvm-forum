{% assign edition = site.editions[-1] %}

{% if edition.place != "Location TBD" %}
## Announcing KVM Forum {{ edition.title }}!
{% else %}
## About KVM Forum
{% endif %}

KVM Forum is an annual event that presents a rare opportunity for
developers and users to discuss the state of Linux virtualization
technology and plan for the challenges ahead. Sessions include updates
on the state of the KVM virtualization stack, planning for the future,
and many opportunities for attendees to collaborate.  Birds of a feather
(BoF) sessions facilitate discussion of strategic decisions.

{% if edition.place != "Location TBD" %}
The next KVM Forum will be held in {{ edition.place }},
on {{ edition.dates }}.
{% else %}
Stay tuned for the dates and location of the next KVM Forum!
{% endif %}
