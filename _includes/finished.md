{% if site.editions[-1].place != "Location TBD" %}
{% assign edition = site.editions[-1] %}
{% else %}
{% assign edition = site.editions[-2] %}
{% endif %}

# Thanks for attending KVM Forum!

You may now watch recorded presentations on our KVM Forum
{{ edition.title }} [YouTube playlist]({{ edition.playlist }}) or
[download slides]({{ edition.title }}).

{% if edition.photos %}
And of course, you may also look at some [pictures]({{ edition.photos }})
of the conference.
{% endif %}
