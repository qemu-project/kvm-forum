---
dates: "August 15-16, 2011"
place: "Vancouver, Canada"
layout: archive
---
## Monday, August 15th

* Avi Kivity, <i>[Keynote]({% attachment 2011-forum-keynote.pdf %})</i>
* Paul Mackerras, <i>[KVM on the IBM POWER7 Processor]({% attachment 2011-forum-KVM_on_the_IBM_POWER7_Processor.pdf %})</i>
* Alex Williamson, <i>[VFIO: PCI device assignment breaks free of KVM]({% attachment 2011-forum-VFIO.pdf %})</i>
* Kevin Wolf, <i>[The reinvention of qcow2]({% attachment 2011-forum-qcow2.pdf %})</i>
* Stefan Hajnoczi & Paolo Bonzini, <i>[Virtio SCSI: An alternative virtualized storage stack for KVM]({% attachment 2011-forum-virtio-scsi.pdf %})</i>
* Asias He, <i>[Native Linux KVM tool]({% attachment 2011-forum-native-linux-kvm-tool.pdf %})</i>
* Andrea Arcangeli, <i>[What's coming from the MM for KVM]({% attachment 2011-forum-aa-numa.pdf %})</i>
* Stuart Yoder, <i>[KVM on Embedded Power Architecture Platforms]({% attachment 2011-forum-embedded-power.pdf %})</i>
* Rik van Riel, <i>[Guest memory resizing - free page hinting & more]({% attachment 2011-forum-memory-overcommit.pdf %})</i>
* Jan Kiszka, <i>[Using KVM as a Real-Time Hypervisor]({% attachment KVM-Forum-2011-RT-KVM.pdf %})</i>
* Mark Wagner, <i>[Optimizing Your KVM Instances]({% attachment Kvm-forum-2011-performance-improvements-optimizations-D.pdf %})</i>
* Bryan Cantrill, <i>[Experiences porting KVM to SmartOS]({% attachment 2011-forum-porting-to-smartos.pdf %})</i>
* Michael S. Tsirkin, <i>[virtio networking status update and case study]({% attachment 2011-forum-virtio_net_whatsnew.odp %})</i>
* Daniel Berrange, <i>[Introduction to the libvirt APIs for KVM management and their future development]({% attachment 2011-forum-libvirt.pdf %})</i>
* Ryan Harper, <i>[IO Throttling in QEMU]({% attachment 2011-forum-keep-a-limit-on-it-io-throttling-in-qemu.pdf %})</i>
* Dan Kenigsberg, <i>[VDSM is now Free]({% attachment Vdsm.pp.pdf %})</i>
* Yoshi Tamura, [The best of both worlds: Network virtualization and KVM]({% attachment 2011-forum-yoshi-kvm-forum-2011.pdf %})
* Dan Magenheimer, [Transcendent Memory and Friends]({% attachment TmemNotVirt-Linuxcon2011-Final.pdf %}) (lightning talk)

## Tuesday, August 16th

* [Keynote]({% attachment 2011-forum-qemu-keynote-liguori.pdf %}), Anthony Liguori
* [Performance monitoring in KVM guests]({% attachment Kvm-forum-2011-performance-monitoring.pdf %}), Avi Kivity
* [AHCI - doing storage right]({% attachment 2011-forum-ahci.pdf %}), Alexander Graf
* [Code Generation for Fun and Profit]({% attachment 2011-forum-qapi-liguori.pdf %}), Anthony Liguori
* [QEMU's device model qdev: Where do we go from here?]({% attachment 2011-forum-armbru-qdev.pdf %}), Markus Armbruster
* [SPICE Roadmap]({% attachment Spice_Roadmap_KVM_forum_2011.pdf %}), Alon Levy
* [Fixing the USB disaster]({% attachment 2011-forum-usb.pdf %}), Gerd Hoffmann
* [KVM Graphics Direct Assignment]({% attachment 2011-forum-%24graphics-direct-assignment.pdf %}), Paul Lu
* [Making KVM autotest useful for KVM developers]({% attachment 2011-forum-lmr-kvm-autoteset.pdf %}), Marcelo Tosatti
* [AMD IOMMU Version 2 Support in KVM]({% attachment 2011-forum-amd-iommuv2-kvm.pdf %}), Jagane Sundar
* [Implementing a Hardware Appliance Product: Applied usage of qemu/KVM and libvirt]({% attachment 2011-kvmforum-hw-appliances-rmatinata.pdf %}), Kei Ohmura
* [Improving the Out-of-box Performance When Using KVM]({% attachment 2011-forum-Improving-out-of-box-performance-v1.4.pdf %}), Benoit Hudzia
* [Yabusame: Postcopy Live Migration for Qemu/KVM]({% attachment 2011-forum-yabusame-postcopy-migration.pdf %}), Takahiro Hirofuchi

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PL7C0F52E2227156B3) and [Archive.org](https://www.archive.org/details/KvmForum2011Presentations)
