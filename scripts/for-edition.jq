    .results | map ({
         "speakers": .speakers | map(.name) | join(", "),
         "time" : .slot.start,
         "room" : .slot.room.en,
         "title": .title,
         "slides": (.resources[0].resource as $name |
            if $name == null then
              []
            else
              [$name | match("[^/]*$").string]
            end) } | select(.speakers != "")) | sort_by(.time)
