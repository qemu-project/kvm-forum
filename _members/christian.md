---
name: Christian Bornträger
role: Senior Technical Staff Member - IBM
---
Christian is the chief product owner for Linux on IBM Z and IBM
LinuxONE. He has contributed to the Linux kernel, KVM, QEMU, valgrind, gdb and
other projects, and is now responsible for the development team working
on kernel, toolchain and KVM. In his role as maintainer for KVM on s390x
he is also looking into KVM across architectures.
