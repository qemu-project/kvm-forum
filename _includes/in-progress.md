{% assign edition = site.editions[-1] %}

# KVM Forum is live!

[Consult the schedule here]({{ edition.schedule }}).

Live streaming is available on YouTube:

* ...

# Upload your slides

Speakers should upload their slides on [Pretalx]({{ edition.submit }}).
To do so, edit your submission and add the slides as a "resource".
Thanks!
