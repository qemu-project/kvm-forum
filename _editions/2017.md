---
dates: "October 25-27, 2017"
place: "Prague, Czech Republic"
layout: archive
talks: [
  {
    "speakers": "Prerna Saxena",
    "time": "2017-10-25T11:15:00+02:00",
    "title": "Lessons in Running libvirtd at Scale",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=acf38ee5WzI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=1&pp=iAQB"
  },
  {
    "speakers": "Paolo Bonzini, Hannes Reinecke",
    "time": "2017-10-25T12:05:00+02:00",
    "title": "OMG, NPIV! Virtualizing Fibre Channel with Linux and KVM",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=ME1IdbtaU5E&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=2&pp=iAQB"
  },
  {
    "speakers": "Alexander Graf",
    "time": "2017-10-25T14:15:00+02:00",
    "title": "QEMU in UEFI",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=uxvAH1Q4Mx0&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=20&pp=iAQB"
  },
  {
    "speakers": "Michael S. Tsirkin, Jens Freimann",
    "time": "2017-10-25T15:05:00+02:00",
    "title": "The Future of virtio: Riddles, Myths and Surprises",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=z9cWwgYH97A&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=3&pp=iAQB"
  },
  {
    "speakers": "Martin Polednik",
    "time": "2017-10-25T16:15:00+02:00",
    "title": "Helping Users Maximize VM Performance",
    "slides": [
      "kvm_forum.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=_SlUlQRcnQg&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=4&pp=iAQB"
  },
  {
    "speakers": "Yang Zhang",
    "time": "2017-10-25T17:05:00+02:00",
    "title": "KVM Performance Tuning on Alibaba Cloud",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=i3kNI7hTF8g&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=42&pp=iAQB"
  },
  {
    "speakers": "Christian Borntraeger",
    "time": "2017-10-26T09:00:00+02:00",
    "title": "Keynote: KVM Status Report",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=S9FpBvoj1zA&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=5&pp=iAQB"
  },
  {
    "speakers": "Christoffer Dall",
    "time": "2017-10-26T09:15:00+02:00",
    "title": "To EL2, and Beyond!",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=j0bp4fnO98w&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=6&pp=iAQB"
  },
  {
    "speakers": "Fabian Deutsch, Roman Mohr",
    "time": "2017-10-26T10:00:00+02:00",
    "title": "Running Virtual Machines on Kubernetes with libvirt & KVM",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=Wh-ejUyuHJ0&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=7&pp=iAQB"
  },
  {
    "speakers": "Max Reitz, Kevin Wolf",
    "time": "2017-10-26T11:15:00+02:00",
    "title": "Managing the New Block Layer",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=9-CA7M8C7wI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=8&pp=iAQB"
  },
  {
    "speakers": "Mihai Donțu",
    "time": "2017-10-26T11:15:00+02:00",
    "title": "Bringing Commercial Grade Virtual Machine Introspection to KVM",
    "slides": [
      "KVMForum2017.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=sUPSogabV-o&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=28&pp=iAQB"
  },
  {
    "speakers": "Maxim Nestratov, Vladimir Sementsov-Ogievskiy",
    "time": "2017-10-26T12:00:00+02:00",
    "title": "Qemu Backup",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=Mp0ATSdxtUY&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=9&pp=iAQB"
  },
  {
    "speakers": "Christoffer Dall",
    "time": "2017-10-26T12:00:00+02:00",
    "title": "Nested Virtualization on ARM",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=cuXyGkZRUKU&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=29&pp=iAQB"
  },
  {
    "speakers": "Yulei Zhang",
    "time": "2017-10-26T14:00:00+02:00",
    "title": "Live Migration with Mediated Device",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=ZAzv0c-fdAc&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=10&pp=iAQB"
  },
  {
    "speakers": "Mike Rapoport",
    "time": "2017-10-26T14:00:00+02:00",
    "title": "Zero-Copy Receive for vhost",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=szA5h7od634&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=30&pp=iAQB"
  },
  {
    "speakers": "Tina Zhang",
    "time": "2017-10-26T14:30:00+02:00",
    "title": "Generic Buffer Sharing Mechanism for Mediated Devices",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=cHMLBcHplhk&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=11&pp=iAQB"
  },
  {
    "speakers": "Pei Zhang",
    "time": "2017-10-26T14:30:00+02:00",
    "title": "Configuring and Benchmarking Open vSwitch, DPDK and vhost-user",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=OnTQRgUyiv8&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=31&pp=iAQB"
  },
  {
    "speakers": "Mike Caraman",
    "time": "2017-10-26T15:00:00+02:00",
    "title": "Adding Device Direct Assignment Support for a New Bus Infrastructure on ARM64: Challenges and Performance Tuning",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=kMKjF2RBKYE&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=12&pp=iAQB"
  },
  {
    "speakers": "Cunming Liang",
    "time": "2017-10-26T15:00:00+02:00",
    "title": "virtio: vhost Data Path Acceleration towards NFV Cloud",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=v50-rXGMr-Y&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=32&pp=iAQB"
  },
  {
    "speakers": "Fam Zheng",
    "time": "2017-10-26T15:30:00+02:00",
    "title": "Userspace NVMe driver in QEMU",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=bwyHxb4tng0&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=13&pp=iAQB"
  },
  {
    "speakers": "Maxime Coquelin",
    "time": "2017-10-26T15:30:00+02:00",
    "title": "Improve VNF Safety with Vhost-User/DPDK IOMMU Support",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=amlhowbtlSw&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=33&pp=iAQB"
  },
  {
    "speakers": "Jim Mattson",
    "time": "2017-10-26T16:30:00+02:00",
    "title": "Where Does the Time Go? Profiling Nested KVM on x86/Intel",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=PxDHNfrpwHE&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=14&pp=iAQB"
  },
  {
    "speakers": "Lei Gong, Xin Zeng",
    "time": "2017-10-26T16:30:00+02:00",
    "title": "virtio-crypto: A New Framework of Cryptography Virtio Device",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=9SYHRYcNa_w&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=34&pp=iAQB"
  },
  {
    "speakers": "Ladi Prosek",
    "time": "2017-10-26T17:00:00+02:00",
    "title": "Nested Virtualization: Hyper-V on KVM",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=zQSkKgpj9jA&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=15&pp=iAQB"
  },
  {
    "speakers": "Cleber Rosa",
    "time": "2017-10-26T17:00:00+02:00",
    "title": "Testing Techniques Applied to Virt Development",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=4cRceasctUI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=36&pp=iAQB"
  },
  {
    "speakers": "Dr. David Alan Gilbert",
    "time": "2017-10-27T09:15:00+02:00",
    "title": "Failing Migrations: How & Why",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=Ku8zgSeGjrM&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=17&pp=iAQB"
  },
  {
    "speakers": "Marc-Andre Lureau, Konrad Rzeszutek Wilk",
    "time": "2017-10-27T10:00:00+02:00",
    "title": "Multi-Process QEMU",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=Kq1-coHh7lg&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=18&pp=iAQB"
  },
  {
    "speakers": "Eric Auger",
    "time": "2017-10-27T11:15:00+02:00",
    "title": "vIOMMU/ARM: Full Emulation and virtio-iommu Approaches",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=7aZAsanbKwI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=19&pp=iAQB"
  },
  {
    "speakers": "Felipe Franciosi, Jim Harris",
    "time": "2017-10-27T11:15:00+02:00",
    "title": "Introducing vhost-user-scsi and It's Applications",
    "slides": [
      "20171027%20-%20KVM%20Forum%20Final.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=DxqEaCPijlI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=35&pp=iAQB"
  },
  {
    "speakers": "Benjamin Serebrin",
    "time": "2017-10-27T12:00:00+02:00",
    "title": "Measuring the Effects of Turbo on VMs",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=kcQfHTGPg2g&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=21&pp=iAQB"
  },
  {
    "speakers": "Marcel Apfelbaum, Yuval Shaia",
    "time": "2017-10-27T12:00:00+02:00",
    "title": "RDMA is Coming to QEMU",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=z3JAS6qluCk&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=38&pp=iAQB"
  },
  {
    "speakers": "Xiao Guangrong",
    "time": "2017-10-27T14:00:00+02:00",
    "title": "Fast Write-Protect and Fast Dirtylog-Bitmap Sync Up",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=kJt348q8OZQ&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=22&pp=iAQB"
  },
  {
    "speakers": "Markus Armbruster",
    "time": "2017-10-27T14:00:00+02:00",
    "title": "Towards a More Expressive and Introspectable QEMU Command Line",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=gtpOLQgnwug&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=37&pp=iAQB"
  },
  {
    "speakers": "Chao Zhang",
    "time": "2017-10-27T14:30:00+02:00",
    "title": "Live Migration at Ali-Cloud: Issues Settled & Challenges Remain",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=-IcHsnQ1S30&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=23&pp=iAQB"
  },
  {
    "speakers": "Alessandro Di Federico",
    "time": "2017-10-27T14:30:00+02:00",
    "title": "libtcg: Exposing QEMU's TCG Frontend to External Tools",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=zCu1pyfSaCY&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=43&pp=iAQB"
  },
  {
    "speakers": "Peter Krempa",
    "time": "2017-10-27T15:30:00+02:00",
    "title": "Keynote: Libvirt Status Report",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=kXMZefakcAI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=24&pp=iAQB"
  },
  {
    "speakers": "Stefan Hajnoczi",
    "time": "2017-10-27T15:45:00+02:00",
    "title": "Applying Polling Techniques to QEMU: Reducing virtio-blk I/O Latency",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=g2Wlia9bo88&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=25&pp=iAQB"
  },
  {
    "speakers": "Pavel Dovgalyuk",
    "time": "2017-10-27T15:45:00+02:00",
    "title": "Instrumenting, Introspection, and Debugging with QEMU",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=3g1KzfBl1kI&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=39&pp=iAQB"
  },
  {
    "speakers": "Alberto Garcia",
    "time": "2017-10-27T16:15:00+02:00",
    "title": "Improving the Performance of the qcow2 Format",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=kmUxIOTiGNo&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=26&pp=iAQB"
  },
  {
    "speakers": "Riku Voipio",
    "time": "2017-10-27T16:15:00+02:00",
    "title": "Linux-User Support in QEMU",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=4MaGnMGPIq0&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=40&pp=iAQB"
  },
  {
    "speakers": "Amit Abir, Eyal Moscovici",
    "time": "2017-10-27T16:45:00+02:00",
    "title": "How to Handle Globally Distributed qcow2 Chains",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=EMK7KVDHSNg&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=27&pp=iAQB"
  },
  {
    "speakers": "Alex Bennée",
    "time": "2017-10-27T16:45:00+02:00",
    "title": "Vectoring in on QEMU's TCG Engine",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=IYHTwnde0g8&list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R&index=41&pp=iAQB"
  }
]
sponsors:
  - type: "Platinum Sponsor"
    who: ["Huawei", "Red Hat (old logo)"]
  - type: "Gold Sponsors"
    who: ["Google Cloud", "IBM", "Intel"]
  - type: "Silver Sponsor"
    who: ["Arm"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2017).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfwG43oL8E_KrzUuSrQvjA_R).

# Pictures

* [Official pictures from the Linux Foundation](https://www.flickr.com/photos/linuxfoundation/albums/72157665450879069)
* [Group photo](https://www.flickr.com/photos/linuxfoundation/24189630688/).
