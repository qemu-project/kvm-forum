KVM Forum 2025 will be held at [Politecnico di Milano](https://maps.app.goo.gl/fsmYbd3YT1v9yQTVA),
Italy.

## Getting around Milan

Milan's public transport system consists of five subway lines and many
tramway and bus lines.

The venue is a 5 minutes walk from the <i>Piola</i> stop of the
"green" M2 subway line.  Downtown Milan can be reached by subway
in about 10 minutes.

The easiest way to use public transport in Milan is to tap a credit
card against the readers every time you board a vehicle or pass the
subway barriers, as well as when you leave the subway.  In particular,
when leaving the subway, note that not all barriers have a credit card
reader and the barriers may be open during peak hours, but you still
have to tap.  The system combines multiple trips taken during a single
day in order to calculate the cheapest fare.  For more information,
see the [ATM contactless paying
web page](https://www.atm.it/en/ViaggiaConNoi/Biglietti/Pages/contactless_cards_metro.aspx).

Subway service ends around 12:30 AM.

## Getting to Milan

The main airport in Milan is Milano Malpensa (MXP).  It is well connected
by trains to the city center and to the subway lines.  Milano Linate
(LIN) is a city airport with a fast connection to downtown via the "blue"
M4 subway line.

Flights are available between the Milan area and most European countries,
as well as from America and Asia to Malpensa.

Another airport, Bergamo (BGY), hosts low-cost airlines and is connected
to the city center by buses.

Milan is also accessible by rail, including high-speed and international
routes.

If you need a visa invitation letter, please reach out to the organizers at
[kvm-forum-pc@redhat.com](mailto:kvm-forum-pc@redhat.com).

## Accomodation

We will provide special room prices for hotels near Politecnico
for KVM Forum attendees.

More information will be posted here or via Eventbrite.
